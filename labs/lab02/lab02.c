#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include "pico/stdlib.h"
//#include "pico/float.h"     // Required for using single-precision variables.
//#include "pico/double.h"    // Required for using double-precision variables.
#define pi 3.14159265359

/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */
float num1()
{
    int i;
    float ans = 2;
    float u;
    for (i=1;i<100000;i++)
    {
        u=(4*pow(i,2))/(4*pow(i,2)-1);
        ans *= u;
    }
    return ans;
}
double num2()
{
    int i;
    double ans = 2;
    double u;
    for (i=1;i<100000;i++)
    {
        u=(4*pow(i,2))/(4*pow(i,2)-1);
        ans *= u;
    }
    return ans;
}
int main() {
    int x;
    printf("Value of Pi using single-precision:- %f\n",num1());
    printf("Error approximation:- %f\n",(pi-num1()));
    printf("Value of Pi using double-precision:- %f\n",num2());
    printf("Error approximation:- %f\n",(pi-num2()));
    
    


    // Returning zero indicates everything went okay.
    return 0;
}
