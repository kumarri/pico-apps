#include "hardware/regs/addressmap.h"
#include "hardware/regs/io_bank0.h"
#include "hardware/regs/timer.h"
#include "hardware/regs/m0plus.h"

.syntax unified
.cpu    cortex-m0plus
.thumb
.global main_asm
.align  4

.equ    DFLT_STATE_STRT, 1            // Specify the value to start flashing
.equ    DFLT_STATE_STOP, 0            // Specify the value to stop flashing
.equ    DFLT_ALARM_TIME, 1000000      // Specify the default alarm timeout

.equ    GPIO_BTN_DN_MSK, 0x00040000   // Bit-18 for falling-edge event on GP20
.equ    GPIO_BTN_EN_MSK, 0x00400000   // Bit-22 for falling-edge event on GP21
.equ    GPIO_BTN_UP_MSK, 0x04000000   // Bit-26 for falling-edge event on GP22

.equ    GPIO_BTN_DN,  20              // Specify pin for the "down" button
.equ    GPIO_BTN_EN,  21              // Specify pin for the "enter" button
.equ    GPIO_BTN_UP,  22              // Specify pin for the "up" button
.equ    GPIO_LED_PIN, 25              // Specify pin for the built-in LED
.equ    GPIO_DIR_IN,   0              // Specify input direction for a GPIO pin
.equ    GPIO_DIR_OUT,  1              // Specify output direction for a GPIO pin

.equ    LED_VAL_ON,    1              // Specify value that turns the LED "on"
.equ    LED_VAL_OFF,   0              // Specify value that turns the LED "off"

.equ    GPIO_ISR_OFFSET, 0x74         // GPIO is int #13 (vector table entry 29)
.equ    ALRM_ISR_OFFSET, 0x40         // ALARM0 is int #0 (vector table entry 16)

// Entry point to the ASM portion of the program
main_asm:
    bl    init_leds           
    bl    init_btns           
    bl    install_alrm_isr    
    bl    install_gpio_isr    
 
loop:
    bl    set_alarm            
    wfi                       
    B     loop               

init_leds:
    push    {lr}                        
    MOVS    R0, #GPIO_LED_PIN           
    BL      asm_gpio_init               
    MOVS    R0, #GPIO_LED_PIN           
    MOVS    R1, #GPIO_DIR_OUT           
    BL      asm_gpio_set_dir            
    pop     {pc}                        

init_btns:
    push    {lr}                        // Store the link register to the stack as we will call nested subroutines
    MOVS    R0, #GPIO_BTN_DN            
    BL      asm_gpio_init               // Call the subroutine to initialise the GPIO pin specified by R0
    MOVS    R0, #GPIO_BTN_DN            // This value is the GPIO LED pin on the PI PICO board
    BL      asm_gpio_set_irq            // Call the subroutine to catch falling edge

    MOVS    R0, #GPIO_BTN_EN            
    BL      asm_gpio_init               
    MOVS    R0, #GPIO_BTN_EN            
    BL      asm_gpio_set_irq            

    MOVS    R0, #GPIO_BTN_UP            
    BL      asm_gpio_init               
    MOVS    R0, #GPIO_BTN_UP            
    BL      asm_gpio_set_irq            // Call the subroutine to catch falling edge

    pop     {pc}                        

install_alrm_isr:
    LDR R2, =(PPB_BASE + M0PLUS_VTOR_OFFSET)    
    LDR R1, [R2]                                
    MOVS R2, #ALRM_ISR_OFFSET                   // Move offset Address to R2
    Add R2, R1                                  
    LDR R0, =alrm_isr                           // Address label alrm_isr is stored in R0
    STR R0, [R2]                                // Pushes the Address back to R0 and stores where we have the offset Address of the table
    MOVS R0,#1                                  // Move the Address of 1 to R0
    LDR R1,=(PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET)
    STR R0,[R1]                                 // Store the value of R0 at the Address of R1
    LDR R1,=(PPB_BASE + M0PLUS_NVIC_ISER_OFFSET)
    STR R0,[R1]                                 // Store the value of R0 at the Address of R1
    BX  LR                                       

install_gpio_isr:
    LDR R2, =PPB_BASE                   // Store base Address in R2        
    LDR R1, =M0PLUS_VTOR_OFFSET         // Store offset Address in R1
    Add R2, R1                          // Add both the Addresses
    LDR R1, [R2]                        
    MOVS R2, #GPIO_ISR_OFFSET           
    Add R2, R1                          
    LDR R0, =gpio_isr                   
    STR R0, [R2]                        
    LDR R0, =8192                       
    LDR R2, =PPB_BASE                   // Store the appropriate Address in R2
    LDR R1, =M0PLUS_NVIC_ICPR_OFFSET    // Store the appropriate Address in R1
    Add R1, R2                         
    STR R0, [R1]                        
    LDR R1, =M0PLUS_NVIC_ISER_OFFSET    // Add offset address at R1
    ADD R1, R2                          // Add both the Addresses
    STR R0, [R1]                        
    BX LR                               // Branch to Link register, Store the Address to return to when the function is done 

set_alarm:
    LDR R2,=TIMER_BASE                                   
    MOVS R1, #1                        
    STR R1, [R2, #TIMER_INTE_OFFSET]      
    LDR R1, [R2, #TIMER_TIMELR_OFFSET]    
    LDR R3, =ltimer                    
    LDR R4, [R3]                       
    Add R1, R4                         
    STR R1, [R2, #TIMER_ALARM0_OFFSET]    
    BX LR                               

.thumb_func                         // Required for all interrupt service routines
alrm_isr:
    push {LR}                       // Store the link register to the stack as we will call nested subroutines
    LDR R2, =TIMER_BASE             // Get the Address of current time in R2 
    MOVS R1, #1                     
    STR R1, [R2, #TIMER_INTR_OFFSET]  // Add #TIMER_INTR_OFFSET and R2, and store the value of R1 at that new Address   
    //Toggling the lights 
    BL sub_toggle                   // Branch with link to sub_toggle
    LDR R0, =ltimer                 // Get the Address of current time in R0
    BL set_alarm                    
    LDR R0, =message4               
    BL printf                       
    pop {pc}                        

 //subroutine used by the alrm_isr
sub_toggle:
    push {lr}                      // Store the link register to the stack as we will call nested subroutines
    LDR R0,=lstate                 
    LDR R3,[R0]                    // Store the value of R0 at the Address of R3
    CMP R3, DFLT_STATE_STOP        // Compare the default stop state with R3
    BEQ endToggle                  // Branch if equal to endToggle
    MOVS R0, #GPIO_LED_PIN         
    BL asm_gpio_get                
    CMP R0, #LED_VAL_OFF           // Check if the LED GPIO pin value is "off"
    BEQ led_set_on                 // If it is "off" then then jump code to turn it on, thus Branch if equal to led_set_on
led_set_off:
    MOVS R1, #LED_VAL_OFF          // The LED is currently "on" so we want to turn it "off"
    B led_set_state                // Branch to state of the LED
led_set_on:
    MOVS R1, #LED_VAL_ON           // The LED is currently "off" so turn it "on"
led_set_state:
    MOVS R0, #GPIO_LED_PIN         // Set the LED GPIO pin number to R0 for use by asm_gpio_put
    BL asm_gpio_put                // Update the the value of the LED GPIO pin (based on value in R1)
endToggle:    
    pop {pc}                       // Pop the link register from the stack to the program counter

.thumb_func
gpio_isr:
    push {LR}                       // Store the link register to the stack as we will call nested subroutines
    LDR R2, =(IO_BANK0_BASE + IO_BANK0_PROC0_INTS2_OFFSET)  // Get the Address of IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET at R2
    LDR R1, [R2]                    // Store the value of R1 at the Address of R2
    LDR R0, =8192                   
    STR R0, [R2]                    // Store the value of R0 at the Address of R2
    LDR R0, =GPIO_BTN_DN_MSK        // Set the approriate mask value to R0
    CMP R1, R0                      // Compare R1 and R0 to see which button is pressed. Checks GP20
    BEQ turnDown                    
    LDR R0, =GPIO_BTN_EN_MSK        // Set the approriate mask value to R1
    CMP R1, R0                      // Compare R1 and R0 to see which button is pressed. Checks GP21
    BEQ turnEnter                   
    LDR R0, =GPIO_BTN_UP_MSK        // Set the approriate mask value to R0
    CMP R1, R0                      // Compare R1 and R0 to see which button is pressed. Checks GP22
    BEQ turnUp                      // Branch if equal to turnUp

gpio_isr_finish:

    pop {pc}                        // Pop the link register from the stack to the program counter

turnDown:
    LDR R2, =lstate                 
    LDR R1, [R2]                    
    MOVS R0, #0                     
    CMP R0, R1                      
    BEQ setDefDN                    // Branch if equal to setDefDN
    LDR R0, =message6                
    BL printf                       
    LDR R2, =ltimer                   
    LDR R1, [R2]                    
    MOVS R0, R1                     // move R1 to R0
    B halving                       // Branch to halving

setDefDN:
    LDR R2, =ltimer                     
    LDR R1, =DFLT_ALARM_TIME            // Get default alarm time in R1
    STR R1, [R2]                        // Store the value of R1 at the Address of R2
    LDR R2, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)    // Get the Address of IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET at R2
    LDR R1, =GPIO_BTN_DN_MSK            
    STR R1, [R2]                        
    LDR R0, =message3                   
    BL printf                           // call the subrotuine to print the value passed by R0
    B gpio_isr_finish                   

halving:
    SUBS R0, #1                         
    MOVS R2, R0                         
    AddS R2, R2                         
    CMP R2, R1                          // compare R2 and R1
    BEQ updateVal_down                  
    BLT updateVal_down                  
    B halving                           

updateVal_down:
    LDR R2, =ltimer                     
    STR R0, [R2]                       // Store the value of R0 at the Address of R2
    LDR R2, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)    
    LDR R1, =GPIO_BTN_DN_MSK           // Set the approriate mask value to R1
    STR R1, [R2]                       
    B gpio_isr_finish                  

turnEnter:
    LDR R2, =lstate                      
    LDR R1, [R2]                        
    MOVS R0, #1                         
    CMP R0, R1                          
    BEQ storeVal0                       
    STR R0, [R2]                        
    LDR R0, =message2                   
    BL printf                           
    MOVS R0, #1                         
    B updateVal_en                      

updateVal_en:
    LDR R2, =lstate                     // Store the current state Address in R2
    LDR R0, [R2]                        // Get the value of the current state
    LDR R2, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)    // Get the Address of IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET at R2
    LDR R1, =GPIO_BTN_EN_MSK              // Set the approriate mask value to R1
    STR R1, [R2]                          // Store the value of R1 at the Address of R2
    B gpio_isr_finish                     // Branch to gpio_isr_finish

storeVal0:
    LDR R0, =message1                   
    BL printf                           
    MOVS R0, #0                         
    B updateVal_en                      

turnUp:
    LDR R2, =lstate                         
    LDR R1, [R2]                        
    MOVS R0, #0                         // Move 0 to R0
    CMP R0, R1                          // Compare R0 with R1
    BEQ setDefaultUP                    
    LDR R2, =ltimer                     
    LDR R1, [R2]                        // Move the Address to R1
    AddS R1, R1                          
    STR R1, [R2]                        // Store the value of R1 at the Address of R2
    LDR R0, =message5                   
    BL printf                           // call the subrotuine to print the value passed by R0
    B updateVal_up                      // Branch to updateVal_up

updateVal_up:
    LDR R2, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET) 
    LDR R1, =GPIO_BTN_UP_MSK            
    STR R1, [R2]                        
    B gpio_isr_finish                   

setDefaultUP:
    LDR R0, =message3                   // store the appropriate message into R0
    BL printf                           
    LDR R2, =ltimer                     // Store the current alarm time at R2
    LDR R1, =DFLT_ALARM_TIME            // Store the (default alarm time) at R1
    STR R1, [R2]                        
    LDR R2, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)    // Get the Address of IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET at R2
    LDR R1, =GPIO_BTN_UP_MSK            // Set the appropriate mask value to R1
    STR R1, [R2]                        
    B gpio_isr_finish                   // Branch to gpio_isr_finish

.align 4
message1:  .asciz  "Flashing Halted.\n"
message2:  .asciz  "Flashing Restarted.\n"
message3:  .asciz  "Blinking Interval Restarted.\n"
message4:  .asciz  "Interrupting the Alarm to toggle the LED.\n"
message5:  .asciz  "Increment in Blinking Interval.\n"
message6:  .asciz  "Decrement in Blinking Interval.\n"

.data
lstate: .word   DFLT_STATE_STRT
ltimer: .word   DFLT_ALARM_TIME
